﻿#ifndef VIDEOPLAYER_H
#define VIDEOPLAYER_H

#include <QWidget>
#include "boderpane.h"
#include "playwnd.h"


class VideoPlayThread;

class VideoPlayer : public BoderPane
{
    Q_OBJECT

public:
    explicit VideoPlayer(QWidget *parent = 0);
    ~VideoPlayer();

private:
    void CreateAllChildWnd();
    void InitCtrl();
    void InitSolts();
    void Relayout();

private slots:
    void OnRecvFrame(QImage imgFrame);

private:
    PlayWnd *m_pPlayWnd;
    VideoPlayThread *m_pPlayThread;
};

#endif // VIDEOPLAYER_H
