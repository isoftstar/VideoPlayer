﻿#ifndef VIDEOPLAYTHREAD_H
#define VIDEOPLAYTHREAD_H

#include <QThread>
#include <QImage>



class VideoPlayThread : public QThread
{
    Q_OBJECT

public:
    VideoPlayThread(QWidget *parent = 0);
    ~VideoPlayThread();

public:
    void StratPlay(QString strVideo);
    void StopPlay();


protected:
    void run();

signals:
    void SignalFrame(QImage imgFrame);

private:
    QString m_strVideoPath;
};

#endif // VIDEOPLAYTHREAD_H
