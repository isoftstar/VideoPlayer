﻿#ifndef PLAYWND_H
#define PLAYWND_H

#include <QWidget>
#include <QImage>

class PlayWnd : public QWidget
{
    Q_OBJECT
public:
    explicit PlayWnd(QWidget *parent = 0);

    void drawTop();
    void mouseMoveEvent(QMouseEvent *event);

private:
    void paintEvent(QPaintEvent *event);

public slots:
    void OnRecvFrame(QImage imgFrame);

private:
    QImage m_imgVideo;

    QImage  m_BottomImg;
    QImage  m_TopImg;
    QPainterPath  m_movePath;
};

#endif // PLAYWND_H
