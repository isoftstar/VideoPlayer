﻿#include "videoplayer.h"
#include "videoplaythread.h"
#include <QImage>
#include <QDebug>
#include <QHBoxLayout>
#include <QVBoxLayout>


VideoPlayer::VideoPlayer(QWidget *parent) :
    BoderPane(parent)
{
    m_pPlayWnd = NULL;
    m_pPlayThread = NULL;

    setWindowFlags(windowFlags() | Qt::FramelessWindowHint | Qt::WindowMinMaxButtonsHint | Qt::Dialog);

    CreateAllChildWnd();
    InitCtrl();
    InitSolts();
    Relayout();
}

VideoPlayer::~VideoPlayer()
{

}

void VideoPlayer::CreateAllChildWnd()
{
#define NEW_OBJECT(pObj, TYPE) \
    if (NULL == pObj) { pObj = new TYPE(this); }

    NEW_OBJECT(m_pPlayWnd, PlayWnd);
    NEW_OBJECT(m_pPlayThread, VideoPlayThread);
}

void VideoPlayer::InitCtrl()
{
    setProperty("canMove", "true");
    setFixedSize(600, 400);
    m_pPlayThread->StratPlay("rtsp://172.16.192.104:554/realtime?chnid=4;vid=1;aid=0;agent=cgi");
}

void VideoPlayer::InitSolts()
{
    connect(m_pPlayThread, &VideoPlayThread::SignalFrame, m_pPlayWnd, &PlayWnd::OnRecvFrame);
}

void VideoPlayer::Relayout()
{
    QVBoxLayout *layoutMain = new QVBoxLayout();
    layoutMain->addWidget(m_pPlayWnd);
    layoutMain->setMargin(0);

    centralWidget()->setLayout(layoutMain);
}

void VideoPlayer::OnRecvFrame(QImage imgFrame)
{
    qDebug() << imgFrame;
}
