﻿#include "playwnd.h"
#include <QDebug>
#include <QPainter>

PlayWnd::PlayWnd(QWidget *parent) : QWidget(parent)
{
    m_movePath.setFillRule(Qt::WindingFill);
}

void PlayWnd::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform | QPainter::TextAntialiasing, true); // 抗锯齿和使用平滑转换算法


    /* 雷达绘制
    QRect m_drawArea;
    if(width()> height())
        m_drawArea =QRect((width()- height())/2,0,height(),height());
    else
        m_drawArea =QRect(0,(height()-width())/2,width(),width());

    m_drawArea.adjust(10,10,-10,-10);

    painter.fillRect(rect(),QColor(15,45,188));
    //边长
    int len = m_drawArea.width();
    //底盘（×轴、y轴和3个圆)
    painter.setPen(QPen(Qt::white));
    painter.drawLine(m_drawArea.topLeft()+ QPoint(0,len/2),m_drawArea.topRight()+ QPoint(0,len/2));
    painter.drawLine(m_drawArea.topLeft()+ QPoint(len/2,0),m_drawArea.bottomLeft()+QPoint(len/2,0));
    painter.drawEllipse(m_drawArea.center(),len/2,len/2);
    painter.drawEllipse(m_drawArea.center(),len/3,len/3);
    painter.drawEllipse(m_drawArea.center(),len/6,len/6);


    int m_pieRotate = -45;
    //转动部分
    //---//线
    qreal x = m_drawArea.center().x()+(qreal)len/2 * cos(-m_pieRotate*3.14159/180);
    qreal y = m_drawArea.center().y()+(qreal)len/2 * sin(-m_pieRotate*3.14159/180);
    painter.setPen(QPen(Qt::white));
    painter.drawLine(m_drawArea.center(),QPointF(x,y));
    //----//扇形
    QConicalGradient gradient;
    gradient.setCenter(m_drawArea.center());
    gradient.setAngle(m_pieRotate+180);
    gradient.setColorAt(0.4,QColor(255,255,255,100));
    gradient.setColorAt(0.8,QColor(255,255,255,0));
    painter.setBrush(QBrush(gradient));
    painter.setPen(Qt::NoPen);
    painter.drawPie(m_drawArea,m_pieRotate*16,90*16);

    return;
    */

    painter.setBrush(Qt::black);
    painter.drawRect(0, 0, this->width(), this->height()); //先画成白色

    if (m_imgVideo.size().width() <= 0)
        return;

    //将图像按比例缩放成和窗口一样大小
    QImage img = m_imgVideo.scaled(this->size(),Qt::KeepAspectRatio);

    int x = this->width() - img.width();
    int y = this->height() - img.height();

    x /= 2;
    y /= 2;

    painter.drawImage(QPoint(x,y), img); //画出图像

    /* 绘制
    drawTop();
    painter.drawImage(rect(), m_TopImg);
    */
}

void PlayWnd::OnRecvFrame(QImage imgFrame)
{
    m_imgVideo = imgFrame;
    update();
}

void PlayWnd::drawTop()
{
    m_TopImg = QImage(size(),QImage::Format_ARGB32_Premultiplied);
    QPainter painter(&m_TopImg);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform | QPainter::TextAntialiasing, true); // 抗锯齿和使用平滑转换算法
    painter.drawPixmap(rect(),QPixmap("C:\\Users\\hudejie\\Music\\music\\cover\\1.png"));
    painter.setCompositionMode(QPainter::CompositionMode_Clear);
    painter.setBrush(Qt::yellow);
    painter.drawPath(m_movePath);
}
#include <QMouseEvent>
void PlayWnd::mouseMoveEvent(QMouseEvent *event)
{
    static QPoint ptLast;

    m_movePath.addEllipse(event->pos(),30,30);

    ptLast = event->pos();

    update();

    return QWidget::mouseMoveEvent(event);
}
