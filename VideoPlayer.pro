#-------------------------------------------------
#
# Project created by QtCreator 2021-06-29T14:20:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VideoPlayer
TEMPLATE = app
DESTDIR = bin

include ($$PWD/public/public.pri)


INCLUDEPATH += $$PWD/ffmpeg/include
INCLUDEPATH += $$PWD/include

SOURCES += source/main.cpp\
        source/videoplayer.cpp \
    source/videoplaythread.cpp \
    source/playwnd.cpp

HEADERS  += include/videoplayer.h \
    include/videoplaythread.h \
    include/playwnd.h

FORMS    += ui/videoplayer.ui

LIBS += $$PWD/ffmpeg/lib/avcodec.lib \
        $$PWD/ffmpeg/lib/avdevice.lib \
        $$PWD/ffmpeg/lib/avfilter.lib \
        $$PWD/ffmpeg/lib/avformat.lib \
        $$PWD/ffmpeg/lib/avutil.lib \
        $$PWD/ffmpeg/lib/postproc.lib \
        $$PWD/ffmpeg/lib/swresample.lib \
        $$PWD/ffmpeg/lib/swscale.lib

DISTFILES +=
